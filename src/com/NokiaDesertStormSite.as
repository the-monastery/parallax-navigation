package com
{
	import caurina.transitions.properties.CurveModifiers;
	import caurina.transitions.properties.DisplayShortcuts;
	import caurina.transitions.properties.FilterShortcuts;
	import caurina.transitions.properties.SoundShortcuts;
	
	import com.indusblue.utils.LoadXML;
	import com.nokia.desertstorm.audio.BGSound;
	import com.nokia.desertstorm.data.ProjectData;
	import com.nokia.desertstorm.events.DesertStormEvent;
	import com.nokia.desertstorm.events.MenuEvent;
	import com.nokia.desertstorm.navigation.Navigation;
	import com.nokia.desertstorm.parallax.Landscape;
	
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;

	public class NokiaDesertStormSite extends Sprite
	{
		private var _background:Background = new Background();
		private var _landscape:Landscape;
		private var _navigation:Navigation;
		private var _xmlData:LoadXML;
		private var _indusLink:IndusblueLink = new IndusblueLink();
		private var _oneMethodLink:OneMethodLink = new OneMethodLink();
				
		//This is an array used to register each button in the Navigation and coordinate with destination markers in the Landscape
		private var _categories:Array = new Array();
		
		public function NokiaDesertStormSite()
		{
			DisplayShortcuts.init();
			CurveModifiers.init();
			SoundShortcuts.init();
			FilterShortcuts.init();
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		private function onAddedToStage(e:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			stage.align = StageAlign.TOP_LEFT;
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.addEventListener(Event.RESIZE, onStageResize);
			
			_xmlData = new LoadXML("flashData/xml/config.xml");
			_xmlData.addEventListener(Event.COMPLETE, onXMLComplete);
		}
		
		private function onXMLComplete(e:Event):void
		{
			_xmlData.removeEventListener(Event.COMPLETE, onXMLComplete);
			ProjectData.createXMLObject(_xmlData.xml);
			
			_landscape  = new Landscape();
			
			/*This will be dispatched after the Center Piece builds in... 
			the start of this chain can be found in the Flash IDE on the last frame of the equalizer_mc */
			_landscape.addEventListener(DesertStormEvent.ANIM_FINISHED, activateMenu);
			
			_background.mouseEnabled = false;
			addChild(_background);
			addChild(_landscape);
			positionExternalLinks();
			
			addChild(_oneMethodLink);
			addChild(_indusLink);
			_oneMethodLink.buttonMode = true;
			_indusLink.buttonMode = true;
			_oneMethodLink.addEventListener(MouseEvent.CLICK, onClick);
			_oneMethodLink.addEventListener(MouseEvent.ROLL_OVER, onRollOver);
			_oneMethodLink.addEventListener(MouseEvent.ROLL_OUT, onRollOut);
			
			_indusLink.addEventListener(MouseEvent.CLICK, onClick);
			_indusLink.addEventListener(MouseEvent.ROLL_OVER, onRollOver);
			_indusLink.addEventListener(MouseEvent.ROLL_OUT, onRollOut);
			
			onStageResize(e);
		}
		
		private function onClick(e:MouseEvent):void
		{
			(e.target == _indusLink)
						? navigateToURL(new URLRequest("http://www.indusblue.com"), "_blank")
						: navigateToURL(new URLRequest("http://www.oneMethod.com"), "_blank");		
		}
		
		private function onRollOver(e:MouseEvent):void
		{
			Landscape.mouseScrollEnabled = false;
		}
		
		private function onRollOut(e:MouseEvent):void
		{
			Landscape.mouseScrollEnabled = true;
		}
		
		private function positionExternalLinks():void
		{
			_oneMethodLink.x = 10;
			_oneMethodLink.y = 10;
			_indusLink.x = stage.stageWidth - (_oneMethodLink.width + 10);
			_indusLink.y = _oneMethodLink.y;
			
		}
		
		private function activateMenu(e:Event):void
		{
			_landscape.removeEventListener(DesertStormEvent.ANIM_FINISHED, activateMenu);
			_navigation = new Navigation();
			addChild(_navigation);
			_navigation.addEventListener(MenuEvent.MENU_CLICK, onMenuClick);
			BGSound.getInstance().start();
		}
		
		//All of this logic decides which button is clicked
		private function onMenuClick(e:MenuEvent):void 
		{
			var who:String = e.data.targetName;
			
			//If the sound button is clicked
			if (who == "soundToggle")
			{
				if(BGSound._nullify == false)
				{
					BGSound.getInstance().silence();
					BGSound._nullify = true;
					_navigation.soundBtn.gotoAndStop(2);
				}
				else
				{
					BGSound.getInstance().amplify();
					BGSound._nullify = false;
					_navigation.soundBtn.gotoAndStop(1);
				}
			}
			//If the centre link is clicked
			else if (who == "centre")
			{
				_landscape.gotoSection(who, 3);
			}
			//If and navigation link is clicked
			else
			{
				_landscape.gotoSection(who);
			}
		}
		
		private function onStageResize(e:Event):void
		{
			//try{removeEventListener(Event.ADDED_TO_STAGE, onStageResize)}catch(e:Error){trace("doesn't exist")};
			_background.width = stage.stageWidth;
			_background.height = stage.stageHeight;
			_landscape.x = stage.stageWidth/2;
			_landscape.y = stage.stageHeight/2; 
			positionExternalLinks();
		}
		
	}
}