package com.nokia.desertstorm.contentviewer
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.NetStatusEvent;
	import flash.media.SoundTransform;
	import flash.media.Video;
	import flash.net.NetConnection;
	import flash.net.NetStream;
	
	public class VideoPlayerOld extends Sprite
	{
		private var _connection:NetConnection = new NetConnection();
		private var _stream:NetStream;
		private var _video:Video = new Video(330,220);
		private var _duration:Number;
		private var _metadataObj:Object = new Object();
		private var _isPlaying:Boolean = false;
		private var _buffer:ProgressMeter = new ProgressMeter();
		
		public function VideoPlayerOld()
		{
			_connection.connect(null);
			_stream = new NetStream(_connection);
			_video.attachNetStream(_stream);
			addChild(_video);
			addChild(_buffer);
			_buffer.x = 165;
			_buffer.y = 110;
		}
		
		public function loadVideo(url:String):void
		{
			_video.clear();
			_stream.play(url);
			_stream.seek(0);
			_stream.pause();
			_stream.client = _metadataObj;
			var transform:SoundTransform = new SoundTransform(0.2);
			_stream.soundTransform = transform;
			_metadataObj.onMetaData = function(metaData:Object):void
			{
				_duration = metaData.duration;
			}
			_stream.bufferTime = 10;
			_stream.addEventListener(NetStatusEvent.NET_STATUS, onNetStatus);
			buttonMode = true;
			addEventListener(MouseEvent.CLICK, videoControl);
		}
		
		private function onNetStatus(e:NetStatusEvent):void
		{
			switch (e.info.code)
			{
				case "NetStream.Buffer.Empty":
				_buffer.start();
				break;
				
			 	case "NetStream.Buffer.Full":
			 	_buffer.stop();
				break;
				
				case "NetStream.Buffer.Flush":
			 	
				break;
				
				case "NetStream.Play.Start":
				
				break;
				
				case "NetStream.Play.Stop":
				
				break;
			}
		}
		
		private function videoControl(e:MouseEvent):void
		{
			if(_isPlaying)
			{
				addEventListener(Event.ENTER_FRAME, trackVideo);
				_stream.pause();
				_isPlaying = false;
			}
			else
			{
				removeEventListener(Event.ENTER_FRAME, trackVideo);
				_stream.resume();
				_isPlaying = true;	
			}
		}
		
		
		public function stopVideo(e:MouseEvent):void
		{
			_stream.pause();
			_stream.seek(0);
			_video.clear();
			addEventListener(MouseEvent.CLICK, videoControl);
		}
		
		private function trackVideo(e:Event):void
		{
			if(_stream.time >= _duration)
			{
				removeEventListener(Event.ENTER_FRAME, trackVideo);
				_stream.pause();
				_stream.seek(0);
				_isPlaying = false;
			}
		}

	}
}