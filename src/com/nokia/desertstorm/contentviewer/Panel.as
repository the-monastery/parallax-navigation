package com.nokia.desertstorm.contentviewer
{
	import caurina.transitions.Tweener;
	
	import com.nokia.desertstorm.audio.BGSound;
	import com.nokia.desertstorm.data.ProjectData;
	import com.nokia.desertstorm.parallax.Landscape;
	
	import fl.controls.UIScrollBar;
	
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	//import NokiaDesertStorm_fla.launch_37;
	import flash.net.navigateToURL;
	import flash.net.URLRequest;

	public class Panel extends Sprite
	{
		private var _background:ContentBG = new ContentBG();
		private var _textContentHolder:ContentText = new ContentText();
		private var _textBody:TextField = _textContentHolder.text;
		private var _head:TextField = _textContentHolder.head;
		private var _subHead:TextField = _textContentHolder.subhead;
		private var _close:Sprite = _textContentHolder.close;
		private var _launchSite:Sprite = _textContentHolder.launchSite;
		private var _scrollBar:UIScrollBar = _textContentHolder.scrollBar;
		
		//private var _video:VideoPlayer = new VideoPlayer();
		private var _video:VideoContainer = new VideoContainer();
		
		private var _image:ImageLoader = new ImageLoader();
		
		public static const IMAGE:String = "image";
		public static const VIDEO:String = "video";
		public static const TEXT:String = "text";
		public static const SCOLL_BAR:String = "scrollBar";
		private var _type:String;
		
		private var _link:String;
		
		public function Panel()
		{
			addEventListener(MouseEvent.ROLL_OVER, focusThis);
			addEventListener(MouseEvent.ROLL_OUT, releaseFocus);
			_close.addEventListener(MouseEvent.CLICK, closePanel);
			_close.buttonMode = true;
			addChild(_background);
			addChild(_textContentHolder);
			
			//_video = new VideoContainer();
			addChild(_video);
			addChild(_image);
			
			_textContentHolder.mouseEnabled = false;
			_background.x = -200;
			_background.y = -310;
			_textContentHolder.x = _background.x;
			_textContentHolder.y = _background.y;
			_video.x = _background.x + 300;
			_video.y = _background.y + 70;
			_image.x = _video.x;
			_image.y = _video.y;
			
			_textContentHolder.scaleX = 0;
			_textContentHolder.scaleY = 0;
			_background.scaleX = 0;
			_background.scaleY = 0;	
			_video.scaleY = 0;
			_video.alpha = 0;
			_image.scaleY = 0;
			_image.alpha = 0;
			initScroller();
		}
		
		public function showPanel(cat:Number, sec:Number):void
		{
			_link = ProjectData.data.group.category[cat].section[sec].link.@url;
			_link == "" ?  disableLaunchSite() : enableLaunchSite();
			_scrollBar.alpha = 0;
			_type = ProjectData.data.group.category[cat].@type;
			var xScale:Number;
			if(_background.scaleY != 1)
			{
				_type == TEXT ? xScale = 1 : xScale = 2.15; 
				
				Tweener.addTween(_background, {scaleY:1, scaleX:xScale, time:1, onComplete:buildInContent, onCompleteParams:[cat, sec]});
				Tweener.addTween(_textContentHolder, {scaleY:1, scaleX:1, time:1});
			}
			else
			{
				buildInContent(cat, sec);
			}
		}
		
		private function enableLaunchSite():void
		{
			Tweener.addTween(_launchSite, {alpha:1, time:0.5});
			_launchSite.addEventListener(MouseEvent.CLICK, onClick);
			_launchSite.buttonMode = true;
		}
		
		private function disableLaunchSite():void
		{
			Tweener.addTween(_launchSite, {alpha:0, time:0.5});
			_launchSite.removeEventListener(MouseEvent.CLICK, onClick);
			_launchSite.buttonMode = false;		
		}
		
		private function onClick(e:MouseEvent):void
		{
			navigateToURL(new URLRequest(_link), "_blank");
		}
		
		private function buildInContent(cat:Number, sec:Number):void
		{	
			_video.videoPlayer.clear();
			var sectionLength:Number = ProjectData.data.group.category[cat].length;
			var sectionNode:XML = ProjectData.data.group.category[cat].section[sec];
			var headerText:String = sectionNode.@name;
			var subHeadText:String = sectionNode.@full;
			var bodyText:String = sectionNode.text;
			
			_textBody.htmlText = bodyText;
			_link = sectionNode.link.@url;
			_link == "" ?  disableLaunchSite(): enableLaunchSite();
			var buildInText:String = _textBody.text;
			_textBody.htmlText = "";
			
			if(_type == VIDEO)
			{
				var videoURL:String = sectionNode.video.@url;
				BGSound._nullify != true ? BGSound.getInstance().silence() : "";
				_video.scaleX = 1;
				_video.scaleY = 1;
				Tweener.addTween(_video, {alpha:1, time:1});
				_video.loadVideo(videoURL);
			}
			else if(_type == IMAGE)
			{
				var imageURL:String = sectionNode.image.@url;
				_image.scaleX = 1;
				_image.scaleY = 1;
				Tweener.addTween(_image, {alpha:1, time:1});
				_image.loadImg(imageURL);	
			}
			
			Tweener.addTween(_head, {_text:headerText, time:0.5});
			Tweener.addTween(_subHead, {_text:subHeadText, time:0.5, delay:0.3});
			Tweener.addTween(_textBody, {_text:buildInText, time:1, delay:0.6, onComplete:setHtmlText});
			
			function setHtmlText():void
			{
				_textBody.htmlText = bodyText;
				initScroller();
			}
			
			/*for(var i:Number = 0; i < sectionLength; i++)
			{
				var subNode:SubNavNode = new SubNavNode();
				addChild(subNode);
			}*/
			
		}
		
		public function closePanel(e:MouseEvent):void
		{
			_video.scaleX = 0;
			_video.scaleY = 0;
			_video.alpha = 0;
			
			BGSound._nullify != true ? BGSound.getInstance().amplify() : "";
			_image.scaleX = 0;
			_image.scaleY = 0;
			_image.alpha = 0;
			
			_video.destroy();
			Tweener.addTween(_background, {scaleY:0, scaleX:0, time:1, onComplete:nullifyPanel});
			Tweener.addTween(_textContentHolder, {scaleY:0, scaleX:0, time:1});
			//Tweener.addTween(_video, {scaleY:0, scaleX:0, time:1});
		}
		
		private function nullifyPanel():void
		{
			_head.text = "";
			_subHead.text = "";
			_textBody.text = "";
		}
		
		private function focusThis(e:MouseEvent):void
		{
			Landscape.mouseScrollEnabled = false;
		}
		
		private function releaseFocus(e:MouseEvent):void
		{
			Landscape.mouseScrollEnabled = true;
		}
		
		private function initScroller():void
		{
			_textBody.maxScrollV != 1 ? Tweener.addTween(_scrollBar,{alpha:1, time:0.5}):_scrollBar.alpha = 0;
			
			_scrollBar.setStyle("scrollBarWidth",6);
			_scrollBar.scrollTarget = _textBody;
			_scrollBar.buttonMode = true;
			_scrollBar.mouseEnabled = true;
			_scrollBar.useHandCursor = true;
			_scrollBar.scrollPosition = 0;
		}
	
	}
}