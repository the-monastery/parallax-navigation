package com.nokia.desertstorm.contentviewer
{
	import com.indusblue.events.VideoPlayerEvent;
	import com.indusblue.media.video.VideoPlayer;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;

	public class VideoContainer extends Sprite
	{
		private var _videoPlayer:VideoPlayer;
		private var _progressMeter:ProgressMeter;
		private var _progressBar:Sprite;
		private var _scrubber:Sprite;
		private var _mouseDown:Boolean;
		private var _playButton:Sprite;
		private var _pauseButton:Sprite;
		private var container:Sprite;
		
		public function get videoPlayer():VideoPlayer{return _videoPlayer};
		
		public function VideoContainer()
		{
			init();
		}
		
		public function destroy():void
		{
			/* Cleaner.removeChild(this, _videoPlayer);
			try
			{
				_videoPlayer.removeEventListener(VideoPlayerEvent.BUFFERING_STARTED, onBufferingStarted);
				_videoPlayer.removeEventListener(VideoPlayerEvent.BUFFERING_STOPPED, onBufferingStopped);
				_videoPlayer.removeEventListener(VideoPlayerEvent.FINISHED, onFinished);
				_videoPlayer.removeEventListener(VideoPlayerEvent.LOADING, onLoading);
				_videoPlayer.removeEventListener(VideoPlayerEvent.STOP, onStop);
				_videoPlayer.removeEventListener(VideoPlayerEvent.PAUSE, onPause);
				_videoPlayer.removeEventListener(VideoPlayerEvent.PLAY, onPlay);
				_videoPlayer.removeEventListener(VideoPlayerEvent.UPDATE, onUpdate);
				_videoPlayer.removeEventListener(VideoPlayerEvent.READY, onReady);
			
			}
			catch(e:Error){}
			
			_videoPlayer = null; */
			
			_videoPlayer.stop();
			container.visible = false;
			
			_scrubber.visible = false;
			_progressBar.visible = false;
		}
		
		public function loadVideo(url:String):void
		{
			container.visible = true;
			
			_scrubber.visible = true;
			_progressBar.visible = true;
			
			_playButton.alpha = 1;
			
			_videoPlayer.load(url);
		}
		
		
		private function init():void
		{
			container = new Sprite();
			container.addEventListener(MouseEvent.MOUSE_OVER, onVideoOver);
			container.addEventListener(MouseEvent.MOUSE_OUT, onVideoOut);
			container.addEventListener(MouseEvent.CLICK, onVideoClick);
			container.mouseChildren = false;
			addChild(container);
			
			_videoPlayer = new VideoPlayer(330, 220, true, 10, false, true);			
			_videoPlayer.addEventListener(VideoPlayerEvent.BUFFERING_STARTED, onBufferingStarted);
			_videoPlayer.addEventListener(VideoPlayerEvent.BUFFERING_STOPPED, onBufferingStopped);
			_videoPlayer.addEventListener(VideoPlayerEvent.FINISHED, onFinished);
			_videoPlayer.addEventListener(VideoPlayerEvent.LOADING, onLoading);
			_videoPlayer.addEventListener(VideoPlayerEvent.STOP, onStop);
			_videoPlayer.addEventListener(VideoPlayerEvent.PAUSE, onPause);
			_videoPlayer.addEventListener(VideoPlayerEvent.PLAY, onPlay);
			_videoPlayer.addEventListener(VideoPlayerEvent.UPDATE, onUpdate);
			_videoPlayer.addEventListener(VideoPlayerEvent.READY, onReady);
			_videoPlayer.addEventListener(MouseEvent.MOUSE_OVER, onVideoOver);
			_videoPlayer.addEventListener(MouseEvent.MOUSE_OUT, onVideoOut);
			
			container.buttonMode = true;
			container.addChild(_videoPlayer);
			
			_progressMeter = new ProgressMeter();
			_progressMeter.x = 330 / 2 - _progressMeter.width;
			_progressMeter.y = 220 / 2 - _progressMeter.height;
			_progressMeter.mouseEnabled = false;
			container.addChild(_progressMeter);
			
			_progressBar = new Sprite();	
			_progressBar.y = 230;		
			addChild(_progressBar);
			
			
			_scrubber = new Sprite();
			_scrubber.y = 229;
			_scrubber.graphics.beginFill(0x666666);
			_scrubber.graphics.drawRect(0, 0, 15, 6);
			_scrubber.graphics.endFill();
			
			_scrubber.addEventListener(MouseEvent.MOUSE_DOWN, onScrubberDown);	
			_scrubber.addEventListener(MouseEvent.MOUSE_UP, onScrubberUp);
			_scrubber.buttonMode = true;
			
			addChild(_scrubber);
			
			_playButton = new ClickToPlay();
			_playButton.x = 330 / 2;
			_playButton.y = 220 / 2;
			_playButton.alpha = 0;
			_playButton.mouseEnabled = false;
			container.addChild(_playButton);
			
			_pauseButton = new ClickToPause();
			_pauseButton.x = 330 / 2 - 2;
			_pauseButton.y = 220 / 2;
			_pauseButton.alpha = 0;
			_pauseButton.mouseEnabled = false;
			container.addChild(_pauseButton);
			
			_mouseDown = false;
			
		}
		
		private function onVideoClick(event:MouseEvent):void
		{
			_videoPlayer.togglePause();
			
			
			
			
			onVideoOver(null);
		}
		
		private function onBufferingStarted(event:VideoPlayerEvent):void
		{
			_progressMeter.start();
		}
		
		private function onBufferingStopped(event:VideoPlayerEvent):void
		{
			_progressMeter.stop();
		}
		
		private function onFinished(event:VideoPlayerEvent):void
		{
			_videoPlayer.pause();
			_progressMeter.stop();
			onVideoOver(null);
		}
		
		private function onLoading(event:VideoPlayerEvent):void
		{
			var progressBarWidth:Number = _videoPlayer.bytesLoaded / _videoPlayer.bytesTotal * 330;
			
			_progressBar.graphics.clear();
			
			_progressBar.graphics.beginFill(0x999999);
			_progressBar.graphics.drawRect(0, 0, progressBarWidth, 4);
			_progressBar.graphics.endFill();
		}
		
		private function onStop(event:VideoPlayerEvent):void
		{
			
		}
		
		private function onPause(event:VideoPlayerEvent):void
		{
			
		}
		
		private function onPlay(event:VideoPlayerEvent):void
		{
			_progressMeter.stop();
		}
		
		private function onUpdate(event:VideoPlayerEvent):void
		{
			var p:Number = _videoPlayer.position / _videoPlayer.duration;
			
			if(!_mouseDown)
			{
				_scrubber.x = Math.floor(p * (330 - _scrubber.width));
			}
		}
		
		private function onReady(event:VideoPlayerEvent):void
		{
			
		}
		
		private function onScrubberDown(event:MouseEvent):void
		{
			addEventListener(Event.ENTER_FRAME, moveScrubber);
			stage.addEventListener(MouseEvent.MOUSE_UP, onScrubberUp);
			
			_mouseDown = true;
		}
		
		private function onScrubberUp(event:MouseEvent):void
		{
			removeEventListener(Event.ENTER_FRAME, moveScrubber);
			stage.addEventListener(MouseEvent.MOUSE_UP, onScrubberUp);
			
			_mouseDown = false;
		}
		
		private function moveScrubber(event:Event):void
		{			
			trace(mouseX);
			_scrubber.x = mouseX - _scrubber.width;
			
			if(_scrubber.x > _progressBar.width - _scrubber.width)
			{
				_scrubber.x = _progressBar.width - _scrubber.width;
			}
			else if(_scrubber.x < 0)
			{
				_scrubber.x = 0;
			}
			
			var p:Number = (_scrubber.x) / _progressBar.width;
						
			_videoPlayer.scrub(p);
		
		}
		
		private function onVideoOver(event:MouseEvent):void
		{
			if(_videoPlayer.state == VideoPlayer.PLAYING)
			{
				_pauseButton.alpha = 1;
				_playButton.alpha = 0;
			}
			else
			{
				_pauseButton.alpha = 0;
				_playButton.alpha = 1;
			}
		}
		
		private function onVideoOut(event:MouseEvent):void
		{
			_pauseButton.alpha = 0;
			_playButton.alpha = 0;
		}
	}
}