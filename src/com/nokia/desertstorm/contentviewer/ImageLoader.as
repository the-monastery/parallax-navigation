package com.nokia.desertstorm.contentviewer
{
	import caurina.transitions.Tweener;
	
	import flash.display.Loader;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.ProgressEvent;
	import flash.net.URLRequest;
	
	public class ImageLoader extends Sprite
	{
		
		private var _loader:Loader = new Loader();
		private var _progress:ProgressMeter = new ProgressMeter();
		
		public function ImageLoader()
		{
			addChild(_loader);
			addChild(_progress);
			_progress.x = 165;
			_progress.y = 110;
		}
		
		public function loadImg(url:String):void
		{
			_loader.alpha = 0;
			_loader.load(new URLRequest(url));
			_loader.contentLoaderInfo.addEventListener(Event.OPEN, onOpen);
			_loader.contentLoaderInfo.addEventListener(ProgressEvent.PROGRESS, onProgress);
			_loader.contentLoaderInfo.addEventListener(Event.COMPLETE, onComplete);
		}
		
		private function onOpen(e:Event):void
		{
			_loader.contentLoaderInfo.removeEventListener(Event.OPEN, onOpen);
			_progress.start();
		}
		
		private function onProgress(e:ProgressEvent):void
		{
			
		}
		
		private function onComplete(e:Event):void
		{
			_progress.stop();
			Tweener.addTween(_loader, {alpha:1, time:0.5});
			_loader.contentLoaderInfo.removeEventListener(ProgressEvent.PROGRESS, onProgress);
			_loader.contentLoaderInfo.removeEventListener(Event.COMPLETE, onComplete);
		}

	}
}