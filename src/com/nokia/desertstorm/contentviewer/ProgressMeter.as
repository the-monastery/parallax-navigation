package com.nokia.desertstorm.contentviewer
{
	import caurina.transitions.Tweener;
	
	import flash.display.Sprite;
	import flash.events.Event;

	public class ProgressMeter extends Sprite
	{
		public function ProgressMeter()
		{
			alpha = 0;
			create();
			this.mouseEnabled = false;	
		}
		
		public function start():void
		{
			addEventListener(Event.ENTER_FRAME, rotator);
			Tweener.addTween(this, {alpha:1, time:0.5});
		}
		
		public function stop():void
		{
			Tweener.addTween(this, {alpha:0, time:0.5, onComplete:removeEnterFrame});
		}
		
		private function removeEnterFrame():void
		{
			removeEventListener(Event.ENTER_FRAME, rotator);	
		}
		
		private function create():void
		{
			for(var i:Number = 1; i < 80; i++) {
				var circle:CircleBuf = new CircleBuf();
				addChild(circle);
				circle.rotation = i * 4.5;
				circle.alpha = 0.8 - i/100;
			}
		}
		
		private function rotator(e:Event):void
		{
			rotation -= 10;
		}
		
	}
}