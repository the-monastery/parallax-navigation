package com.nokia.desertstorm.events
{
	import flash.events.Event;

	public class MenuEvent extends Event
	{
		public static const MENU_CLICK:String = "menuclicked";
		public var data:Object;
		
		public function MenuEvent(type:String, data:Object = null, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			this.data = data;
		}
		
	}
}