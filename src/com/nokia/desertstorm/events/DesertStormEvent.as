package com.nokia.desertstorm.events
{
	import flash.events.Event;

	public class DesertStormEvent extends Event
	{
		//This class simple contains a bunch of useful custom events
		public static const ACTIVE_NAV:String = "activenavigation";
		public static const INACTIVE_NAV:String = "inactivenavigation";
		public static const ANIM_FINISHED:String = "animfinished";
		public static const BUILD_IN_FINISHED:String = "buildInFinished";
		
		public function DesertStormEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
	}
}