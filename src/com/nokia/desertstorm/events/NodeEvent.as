package com.nokia.desertstorm.events
{
	import flash.events.Event;

	public class NodeEvent extends Event
	{
		public static const NODE_CLICKED:String = "nodeclicked";
		public var category:Number;
		public var section:Number;
		
		public function NodeEvent(type:String, cat:Number, sec:Number, bubbles:Boolean=true, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			category = cat;
			section = sec;
			
		}
		
	}
}