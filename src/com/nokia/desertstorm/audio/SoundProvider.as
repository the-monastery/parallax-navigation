package com.nokia.desertstorm.audio
{
	import com.indusblue.media.audio.SoundManager;
	
	public class SoundProvider
	{
		private static var _instance:SoundProvider;
		private var _rolloverSounds:Array = [TickC];
		private var _clickSounds:Array = [DingA, DingB, DingC];
		
		public function SoundProvider()
		{
			
		}
		
		public function playRolloverSound():void
		{
			SoundManager.getInstance().playSound(_rolloverSounds[Math.floor(Math.random()*_rolloverSounds.length)]);
		}
		
		public function playClickSound():void
		{
			SoundManager.getInstance().playSound(_clickSounds[Math.floor(Math.random()*_clickSounds.length)]);
		}
		
		public function playBuildIn():void
		{

			SoundManager.getInstance().playSound(Explosion);
		}
		
		public function playBuildOut():void
		{

		}
		
		static public function getInstance():SoundProvider 
		{
			if(_instance == null)
			{
				return new SoundProvider();
			} 
			else 
			{
				return _instance;
			}
		}

	}
}