package com.nokia.desertstorm.audio
{
	import caurina.transitions.Tweener;
	
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;
	
	public class BGSound
	{
		private static var _instance:BGSound;
		private var _sound:Sound = new BG as Sound;
		private var _soundChannel:SoundChannel;
		public static var _nullify:Boolean = false;
		
		public function BGSound(){}
		
		public function start():void
		{
			var soundTransform:SoundTransform = new SoundTransform(0);
			_soundChannel = _sound.play(0,3000,soundTransform);
			Tweener.addTween(_soundChannel, {_sound_volume:1, time:2});
		}
		
		public function silence():void
		{
			Tweener.addTween(_soundChannel, {_sound_volume:0, time:2});
		}
		
		public function amplify():void
		{
			Tweener.addTween(_soundChannel, {_sound_volume:1, time:2});
		}
		
		static public function getInstance():BGSound 
		{
			if(_instance == null)
			{
				_instance = new BGSound();
				return _instance;
			} 
			else 
			{
				return _instance;
			}
		}

	}
}