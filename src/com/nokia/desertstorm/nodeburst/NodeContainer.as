package com.nokia.desertstorm.nodeburst
{
	import caurina.transitions.Equations;
	import caurina.transitions.Tweener;
	
	import com.nokia.desertstorm.audio.SoundProvider;
	import com.nokia.desertstorm.data.ProjectData;
	
	import flash.display.Sprite;
	import flash.events.Event;

	public class NodeContainer extends Sprite
	{
		private var _nodeArray:Array = new Array();
		
		public function NodeContainer()
		{
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		private function onAddedToStage(e:Event):void
		{
			for(var i:Number = 0; i<ProjectData.data.group.category.length(); i++)
			{
				var n:Number = ProjectData.data.group.category[i].section.length();
				var a:Array = new Array();
				for each(var sectionName:XML in ProjectData.data.group.category[i].section)
				{
					a.push(sectionName.@name);
				}
				var newNode:NodeBurst = new NodeBurst(n, a, i);
				addChild(newNode);
				newNode.alpha = 0;
				newNode.y = 80;
				_nodeArray[ProjectData.data.group.category[i].@name] = newNode;
			}
		}
		
		public function displayNode(section:String):void
		{
			_nodeArray[section].x = 0;
			_nodeArray[section].y = 80;
			Tweener.addTween(_nodeArray[section], {alpha:1, 
													x:-270, 
													y:-220, 
													_bezier:{x:-450, y:-70}, 
													time:1, 
													transition:Equations.easeOutCirc});
			SoundProvider.getInstance().playBuildIn();
			_nodeArray[section].openNode();
		}
		
		public function hideNode(section:String):void
		{
			Tweener.addTween(_nodeArray[section], {alpha:0, 
													x:200, 
													y:-300, 
													_bezier:{x:-50, y:-350}, 
													time:1, 
													transition:Equations.easeOutCirc});
			_nodeArray[section].closeNode();
		}
		
	}
}