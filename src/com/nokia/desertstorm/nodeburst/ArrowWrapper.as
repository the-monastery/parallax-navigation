package com.nokia.desertstorm.nodeburst
{
	import flash.display.Sprite;

	public class ArrowWrapper extends Sprite
	{
		private var _arrow:Arrow = new Arrow;
		private var _category:Number;
		private var _section:Number;
		
		public function get instance():Arrow{return _arrow}
		public function get category():Number{return _category}
		public function get section():Number{return _section}
		
		public function ArrowWrapper(category:Number, section:Number)
		{
			_category = category;
			_section = section;
			addChild(_arrow);
		}
		
	}
}