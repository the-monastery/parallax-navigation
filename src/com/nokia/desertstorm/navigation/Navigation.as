package com.nokia.desertstorm.navigation
{
	import caurina.transitions.Tweener;
	
	import com.indusblue.texteffects.TypeInOut;
	import com.nokia.desertstorm.audio.SoundProvider;
	import com.nokia.desertstorm.data.ProjectData;
	import com.nokia.desertstorm.events.MenuEvent;
	import com.nokia.desertstorm.parallax.Landscape;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	
	public class Navigation extends Sprite
	{
		private var _menu:Menu = new Menu();
		private var _menuItems:Array = new Array();
		private var _soundBtn:MovieClip;
		
		public function get soundBtn():MovieClip{return _soundBtn}
		
		public function Navigation()
		{
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			addEventListener(MouseEvent.ROLL_OVER, onRollOver);
			addEventListener(MouseEvent.ROLL_OUT, onRollOut);
		}
		
		private function onRollOver(e:MouseEvent):void
		{
			//Event dispatched to disable stage listener
			Landscape.mouseScrollEnabled = false;
			Tweener.addTween(_menu, {y:stage.stageHeight - (_menu.height - 15), time:0.5});
		}
		
		private function onRollOut(e:MouseEvent):void
		{
			//Event dispatched to enable stage listener
			Landscape.mouseScrollEnabled = true;
			Tweener.addTween(_menu, {y:stage.stageHeight - 100, time:0.5});
		}
		
		private function onAddedToStage(e:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			stage.addEventListener(Event.RESIZE, onStageResize);
			onStageResize();
			addChild(_menu);
			
			for(var i:int = 0; i < ProjectData.data.group.category.length(); i++)
			{
				var current:String = ProjectData.data.group.category[i].@name;
				var item:MovieClip = _menu.getChildByName(current) as MovieClip;
				var textField:TextField = item.getChildByName("txt") as TextField;
				var text:String = textField.text;
				var menuItem:Object = {itm:item, txt:text};
				
				textField.mouseEnabled = false; 
				menuItem.itm.addEventListener(MouseEvent.CLICK, onClick);
				menuItem.itm.addEventListener(MouseEvent.ROLL_OUT, onBtnRollout);
				menuItem.itm.addEventListener(MouseEvent.ROLL_OVER, onBtnRollover);
				item.mouseChildren = false;
				menuItem.itm.buttonMode = true;
				menuItem.itm.stop();
				_menuItems[current] = menuItem;
			}
			
			_soundBtn = _menu.getChildByName("soundToggle") as MovieClip;
			_soundBtn.addEventListener(MouseEvent.CLICK, onClick);
			_soundBtn.buttonMode = true;
			
			_menu.centre.addEventListener(MouseEvent.CLICK, onClick);
			_menu.centre.buttonMode = true;
			
		}
		
		private function onClick(e:MouseEvent):void
		{
			dispatchEvent(new MenuEvent(MenuEvent.MENU_CLICK, {targetName:e.target.name}));
			SoundProvider.getInstance().playClickSound();	
		}
		
		private function onBtnRollover(e:MouseEvent):void
		{
			var destText:String = _menuItems[e.target.name].txt;
			Tweener.addTween(e.target, {_frame:MovieClip(e.target).totalFrames, time:0.5});
			TypeInOut.addTween(e.target.getChildByName("txt"), {type:TypeInOut.SHUFFLE, text:destText, time:0.5});
			SoundProvider.getInstance().playRolloverSound();
		}
		
		private function onBtnRollout(e:MouseEvent):void
		{
			var destText:String = _menuItems[e.target.name].txt;
			Tweener.addTween(e.target, {_frame:0, time:0.5});
			TypeInOut.addTween(e.target.getChildByName("txt"), {type:TypeInOut.SHUFFLE, text:destText, newColor:"999999", time:0.5});
		}
		
		private function onStageResize(e:Event=null):void
		{
			_menu.x =(stage.stageWidth - _menu.width)/2;
			_menu.y = stage.stageHeight - 100;
		}

	}
}