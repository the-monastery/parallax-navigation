package com.nokia.desertstorm {
	
	import caurina.transitions.Tweener;
	
	import com.ghostmonk.utils.SiteLoader;
	
	import flash.display.MovieClip;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	
	/**
	 *@author ghostmonk - Sep 20, 2011
	 */
	public class DesertStormLoader extends SiteLoader 
	{
		private var preloader:MovieClip;
		
		public function DesertStormLoader()
		{
			super( "NokiaDesertStorm" );
			
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.align = StageAlign.TOP_LEFT;
			
			preloader = new Preloader();
			preloader.alpha = 0;
			addChild(preloader);
			onStageResize( null );
			stage.addEventListener( Event.RESIZE, onStageResize );
			Tweener.addTween( preloader, {alpha:1, time:1} );
		}
		
		override protected function updateLoader( percent:Number ) : void 
		{	
			preloader.gotoAndStop( Math.max( 1, percent * 25 ) );
		}
		
		override protected function cleanUp() : void 
		{
			Tweener.addTween(preloader, {alpha:0, time:1, onComplete:removePreloader});
		}
		
		private function onStageResize( e:Event ) : void
		{
			preloader.x = (stage.stageWidth - preloader.width)/2;
			preloader.y = (stage.stageHeight - preloader.height)/2;
		}
		
		private function removePreloader():void
		{
			removeChild(preloader);
		}
	}
}