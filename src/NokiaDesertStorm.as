package 
{
	import com.NokiaDesertStormSite;
	
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	
	[SWF ( backgroundColor=0xFFFFFF, frameRate=31, width=960, height=700, pageTitle="Nokia Desert Storm" ) ]
	[Frame ( factoryClass="com.nokia.desertstorm.DesertStormLoader" ) ]
	public class NokiaDesertStorm extends Sprite
	{	
		private var _nokiaDesertStormSite:NokiaDesertStormSite = new NokiaDesertStormSite();
		
		public function NokiaDesertStorm()
		{
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		private function onAddedToStage(e:Event):void
		{
			stage.align = StageAlign.TOP_LEFT;
			stage.scaleMode = StageScaleMode.NO_SCALE;
			
			addChild(_nokiaDesertStormSite);
		}
		
	}
}